# -*- coding: utf-8 -*-

from string import Template

# 1. "%s %x" % (var1, var2) üsulu ilə username və orderNo dəyişənlərini salamlama mətninə əlavə edərək istifadəçini salamlayın
username = input("Adınızı daxil edin: ")
orderNo = input("Sifariş nömrənizi daxil edin: ")
greetingMessage = "Hörmətli %s, xoş gəlmisiniz. Sizin sifarişiniz: 0x%x" % # kodun davamını yazın
print("1.", greetingMessage)

# 2. "%(var1)s %(var2)x" % (var1, var2) üsulu ilə username və orderNo dəyişənlərini salamlama mətninə əlavə edərək istifadəçini salamlayın
username = input("Adınızı daxil edin: ")
orderNo = input("Sifariş nömrənizi daxil edin: ")
# kodu buraya yazın

# 3. "{} {:x}".format(var1, var2) metodunun köməkliyi ilə istifadəçinin salamlayın
username = input("Adınızı daxil edin: ")
orderNo = input("Sifariş nömrənizi daxil edin: ")
# kodu buraya yazın

# 4. "{var1} {var2:x}".format(var1=var1, var2=var2) metodunun köməkliyi ilə istifadəçinin salamlayın
username = input("Adınızı daxil edin: ")
orderNo = input("Sifariş nömrənizi daxil edin: ")
# kodu buraya yazın

# 5. f"{var1} {var2:#x}" metodunun köməkliyi ilə istifadəçinin salamlayın
username = input("Adınızı daxil edin: ")
age = int(input("Doğulduğunuz ili daxil edin: "))
orderNo = int(input("Sifariş nömrənizi daxil edin: "))
# kodu buraya yazın

# 6. Template("$var").substitude(var=var) metodunun köməkliyi ilə istifadəçinin salamlayın
username = input("Adınızı daxil edin: ")
age = int(input("Doğulduğunuz ili daxil edin: "))
orderNo = int(input("Sifariş nömrənizi daxil edin: "))
# kodu buraya yazın