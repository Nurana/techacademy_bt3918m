# -*- coding: utf-8 -*-

### Funksiyalar haqda daha ətraflı python dilinin rəsmi dokumentasiya səhifəsindən öyrənə bilərsiniz:
### Link: https://docs.python.org/2/library/string.html#string-functions


# =========================================================================
# 1. istifadəçidən ayrı-ayrılıqda onun adını, soyadını və yaşını soruşun. 
# 2. Daxil edilən ad və soyadın yalnızca hərflərdən ibarət olub olmadığını yoxlayın və print edin
# 2. Daha sonra adın və soyadın ilk hərflərini böyüdün və salamlama mətni ilə birgə print edin
# 3. Ad və soyadın ayrı ayrılıqda uzunluqlarını hesablayıb print edin
# yaşın rəqəm olub olmamasını yoxlayın və nəticəni print edin
# istifadəçinin yaşını print edin
# =========================================================================